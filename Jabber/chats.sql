-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.5-10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2017-11-14 16:49:02
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for chats
DROP DATABASE IF EXISTS `chats`;
CREATE DATABASE IF NOT EXISTS `chats` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `chats`;


-- Dumping structure for table chats.calendar
CREATE TABLE IF NOT EXISTS `calendar` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` varchar(45) NOT NULL,
  `end_date` varchar(45) NOT NULL,
  `text` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table chats.calendar: ~0 rows (approximately)
/*!40000 ALTER TABLE `calendar` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar` ENABLE KEYS */;


-- Dumping structure for table chats.conversation
CREATE TABLE IF NOT EXISTS `conversation` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `from_id` varchar(200) NOT NULL,
  `to_id` varchar(200) NOT NULL,
  `timestamp` varchar(200) NOT NULL,
  `con_id` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- Dumping data for table chats.conversation: ~27 rows (approximately)
/*!40000 ALTER TABLE `conversation` DISABLE KEYS */;
REPLACE INTO `conversation` (`id`, `from_id`, `to_id`, `timestamp`, `con_id`) VALUES
	(1, '2', '1', '1507512035', 0),
	(2, '1', '2', '1507512083', 0),
	(3, '2', '1', '1507861655', 0),
	(4, '1', '2', '1507861669', 0),
	(5, '2', '1', '1507861729', 0),
	(6, '1', '2', '1507861745', 0),
	(7, '2', '1', '1507861788', 0),
	(8, '1', '2', '1507861829', 0),
	(9, '1', '2', '1507862054', 0),
	(10, '1', '2', '1507862215', 0),
	(11, '1', '2', '1508088568', 0),
	(12, '1', '2', '1508940615', 0),
	(13, '1', '2', '1509981077', 0),
	(14, '2', '1', '1509981123', 0),
	(15, '1', '4', '1510704739', 1),
	(16, '1', '4', '1510704739', 1),
	(17, '1', '4', '1510704740', 1),
	(18, '1', '4', '1510704740', 1),
	(19, '1', '4', '1510704740', 1),
	(20, '1', '2', '1510704745', 0),
	(21, '1', '4', '1510704750', 1),
	(22, '4', '2', '1510704806', 2),
	(23, '4', '2', '1510704806', 2),
	(24, '4', '2', '1510704807', 2),
	(25, '4', '2', '1510704817', 2),
	(26, '4', '2', '1510704818', 2),
	(27, '1', '4', '1510704844', 1);
/*!40000 ALTER TABLE `conversation` ENABLE KEYS */;


-- Dumping structure for table chats.conversation_group_reply
CREATE TABLE IF NOT EXISTS `conversation_group_reply` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `reply` tinytext,
  `from_id` int(10) DEFAULT NULL,
  `room_id` int(10) DEFAULT NULL,
  `timestamp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

-- Dumping data for table chats.conversation_group_reply: ~8 rows (approximately)
/*!40000 ALTER TABLE `conversation_group_reply` DISABLE KEYS */;
REPLACE INTO `conversation_group_reply` (`id`, `reply`, `from_id`, `room_id`, `timestamp`) VALUES
	(47, 'kkkk', 1, 15, '1510705585'),
	(48, 'asd', 1, 16, '1510705636'),
	(49, 'ssssss', 1, 17, '1510705714'),
	(50, 'bbbbb', 1, 18, '1510705829'),
	(51, '???', 2, 18, '1510705936'),
	(52, 'broken??', 2, 18, '1510705945'),
	(53, 'dddd', 8, 16, '1510706784'),
	(54, 'lskdvlksdnvlksdv', 7, 16, '1510706802');
/*!40000 ALTER TABLE `conversation_group_reply` ENABLE KEYS */;


-- Dumping structure for table chats.conversation_reply
CREATE TABLE IF NOT EXISTS `conversation_reply` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `reply` text NOT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `timestamp` varchar(500) NOT NULL,
  `con_id` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- Dumping data for table chats.conversation_reply: ~27 rows (approximately)
/*!40000 ALTER TABLE `conversation_reply` DISABLE KEYS */;
REPLACE INTO `conversation_reply` (`id`, `reply`, `from_id`, `to_id`, `timestamp`, `con_id`) VALUES
	(1, 'hi', 2, 1, '1507512035', 0),
	(2, 'Hi', 1, 2, '1507512083', 0),
	(3, 'How are you', 2, 1, '1507861655', 0),
	(4, 'Fine', 1, 2, '1507861669', 0),
	(5, 'So how is your project', 2, 1, '1507861729', 0),
	(6, 'Its Going Good', 1, 2, '1507861745', 0),
	(7, 'What is your project about?', 2, 1, '1507861788', 0),
	(8, 'My Project is about chat application', 1, 2, '1507861829', 0),
	(9, 'where users in an organization can chat with an individual user', 1, 2, '1507862054', 0),
	(10, 'they can also create an event to remind the user', 1, 2, '1507862215', 0),
	(11, 'yo..conncted', 1, 2, '1508088568', 0),
	(12, 'hello', 1, 2, '1508940615', 0),
	(13, 'hello', 1, 2, '1509981077', 0),
	(14, 'HOw are you today?', 2, 1, '1509981123', 0),
	(15, 'wefwef', 1, 4, '1510704739', 1),
	(16, 'wefwef', 1, 4, '1510704740', 1),
	(17, 'wefwef', 1, 4, '1510704740', 1),
	(18, 'wefwef', 1, 4, '1510704740', 1),
	(19, 'wefwef', 1, 4, '1510704740', 1),
	(20, 'wefwefsdfsd', 1, 2, '1510704745', 0),
	(21, 'sdfsdfsdf', 1, 4, '1510704750', 1),
	(22, 'wqeqwe', 4, 2, '1510704806', 2),
	(23, 'wqeqwe', 4, 2, '1510704807', 2),
	(24, 'wqeqwe', 4, 2, '1510704807', 2),
	(25, 'qqq', 4, 2, '1510704818', 2),
	(26, 'qqq', 4, 2, '1510704818', 2),
	(27, 'dfsdf', 1, 4, '1510704844', 1);
/*!40000 ALTER TABLE `conversation_reply` ENABLE KEYS */;


-- Dumping structure for table chats.room
CREATE TABLE IF NOT EXISTS `room` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) DEFAULT NULL,
  `from_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table chats.room: ~4 rows (approximately)
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
REPLACE INTO `room` (`id`, `name`, `from_id`) VALUES
	(15, 'kim', NULL),
	(16, 'Mmm', NULL),
	(17, 'aaa', NULL),
	(18, 'BBB', NULL);
/*!40000 ALTER TABLE `room` ENABLE KEYS */;


-- Dumping structure for table chats.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `p_photo` varchar(200) NOT NULL,
  `timestamp` int(255) NOT NULL,
  `online` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table chats.user: ~3 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`id`, `name`, `password`, `p_photo`, `timestamp`, `online`) VALUES
	(7, 'aaa', '123', '/uploads/1510706545402secondarytile.png', 1510706545, 'Y'),
	(8, 'bbb', '123', '/uploads/1510706602257spot.png', 1510706602, 'N'),
	(9, 'ccc', '1234', '/uploads/1510706642830favicon.png', 1510706642, 'Y');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
